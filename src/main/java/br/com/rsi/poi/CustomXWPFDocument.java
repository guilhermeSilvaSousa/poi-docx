package br.com.rsi.poi;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigInteger;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.model.XWPFHeaderFooterPolicy;
import org.apache.poi.xwpf.usermodel.Borders;
import org.apache.poi.xwpf.usermodel.BreakType;
import org.apache.poi.xwpf.usermodel.ParagraphAlignment;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFHeader;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.apache.poi.xwpf.usermodel.XWPFTable;
import org.apache.poi.xwpf.usermodel.XWPFTableCell;
import org.apache.poi.xwpf.usermodel.XWPFTableRow;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTPageMar;
import org.openxmlformats.schemas.wordprocessingml.x2006.main.CTSectPr;

public class CustomXWPFDocument extends XWPFDocument {

	private Integer testErrorsQuantity;

	public CustomXWPFDocument(InputStream document, String suiteName, String started_at, String finished_at, Integer passedTestsQuantity,
			Integer failedTestsQuantity) throws InvalidFormatException, FileNotFoundException, IOException {
		super(document);
		setTestErrorsQuantity(0);
		//this.setHeader("Suite de Teste Vidalink");
		this.setDocumentMargin();
		setDocumentDate();
		this.setInitInformation(suiteName, String.valueOf(passedTestsQuantity), String.valueOf(failedTestsQuantity), started_at,
				finished_at);
	}

	private XWPFHeaderFooterPolicy headerFooterPolicy;

	public void saveDocument() {
		try {
			
			this.write(new FileOutputStream(getFileName() + ".docx"));
			this.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private String getFileName(){
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		java.util.Date now = new java.util.Date();
		String date = format.format(now);
		format = new SimpleDateFormat("HH_mm_ss");
		String time = format.format(now);
		return "relatorio_testes_automatizados_" +date + "_" + time; 
	}

	private void setDocumentDate() {
		java.util.Date agora = new java.util.Date();
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		String data = format.format(agora);
		this.changeKey("DATE", data);
	}

	private void setInitInformation(String suiteName, String passedTestsQuantity, String failedTestsQuantity, String started_at,
			String finished_at) {
		changeKey("SUITE_NAME", suiteName);
		changeKey("PASSED_TESTS_QTD", passedTestsQuantity);
		changeKey("FAILED_TESTS_QTD", failedTestsQuantity);
		changeKey("TOTAL_TESTS_QTD", String
				.valueOf(Integer.parseInt(failedTestsQuantity.trim()) + Integer.parseInt(passedTestsQuantity.trim())));
		changeKey("STARTED_AT", started_at);
		changeKey("FINISHED_AT", finished_at);
		this.createParagraph().createRun().addBreak(BreakType.PAGE);
	}

	public void changeKey(String key, String to) {

		String paragraphText = null;

		for (XWPFTable table : this.getTables()) {
			for (XWPFTableRow row : table.getRows()) {
				for (XWPFTableCell cell : row.getTableCells()) {
					if (cell.getText().contains("@" + key)) {
						String cellText = cell.getText();
						cell.removeParagraph(0);
						XWPFParagraph paragraph = cell.addParagraph();
						XWPFRun run = paragraph.createRun();
						run.setText(cellText.replace("@" + key, to));
						paragraph.setAlignment(ParagraphAlignment.CENTER);
						return;
					}
				}
			}
		}

		for (XWPFParagraph p : this.getParagraphs()) {
			/*
			 * List<XWPFRun> runs = p.getRuns(); int runTextPosition = 0; if
			 * (runs != null) { for s) (XWPFRun r : run{ String text =
			 * r.getText(0); runTextPosition = r.getTextPosition(); if (text !=
			 * null && text.contains("@" + key)) { sb.append(text);
			 * p.removeRun(runTextPosition); text = text.replace("@" + key, to);
			 * r.setText(text, 0); return; } } }
			 */
			paragraphText = getParagraphText(p);
			if (paragraphText.contains("@" + key)) {
				//removeAllRuns(p);
				//p.createRun().setText(paragraphText.replace("@" + key, to));
				p.getRuns().get(0).setText(paragraphText.replace("@" + key, to), 0);
				p.removeRun(1);
				return;
			}
		}
	}

	private void removeAllRuns(XWPFParagraph paragraph) {
		int quantityOfRuns = paragraph.getRuns().size();
		for (int i = 0; i < quantityOfRuns; i++) {
			paragraph.removeRun(i);
		}
	}

	private String getParagraphText(XWPFParagraph paragraph) {
		StringBuilder sb = new StringBuilder();
		List<XWPFRun> runs = paragraph.getRuns();
		String line = null;
		int runPosition = 0;
		if (runs != null)
			/*
			 * for (XWPFRun run : runs) { while ((line =
			 * run.getText(runPosition)) != null) { sb.append(line);
			 * runPosition++; } }
			 */
			for (XWPFRun run : runs) {
				sb.append(run.getText(0));
			}
		return sb.toString();
	}

	private void setHeader(String suiteName) {
		this.headerFooterPolicy = this.getHeaderFooterPolicy();
		if (this.headerFooterPolicy == null)
			this.headerFooterPolicy = this.createHeaderFooterPolicy();
		XWPFHeader header = this.headerFooterPolicy.getDefaultHeader();
		for (XWPFParagraph paragraph : header.getParagraphs()) {
			List<XWPFRun> runs = paragraph.getRuns();
			if (runs != null)
				for (XWPFRun run : runs) {
					String runContent = run.getText(0);
					if (runContent != null && runContent.contains("@SUITE_NAME")) {
						run.setText(runContent.replace("@SUITE_NAME", suiteName), 0);
						break;
					}
				}
		}
	}

	private void setDocumentMargin() {
		CTSectPr sectPr = this.getDocument().getBody().addNewSectPr();
		CTPageMar pageMar = sectPr.addNewPgMar();
		pageMar.setLeft(BigInteger.valueOf(720L));
		pageMar.setTop(BigInteger.valueOf(360L));
		pageMar.setRight(BigInteger.valueOf(720L));
		pageMar.setBottom(BigInteger.valueOf(360L));
	}


	
	private String extractErrorDetail(String errorMessage){
		Pattern pattern = Pattern.compile(".*failed:(.*)\\s*expected.*");
		Matcher matcher = null;
		if((matcher = pattern.matcher(errorMessage)).matches())
			return matcher.group(1);
		return errorMessage;
	}
	
	/**
	 * 
	 * Usado para adicionar erros em testes voltados para mobile (android)
	 * 
	 * @param deviceName
	 *            Nome do dispositivo
	 * @param testCaseName
	 *            Nome do caso de teste
	 * @param screenshot
	 *            Localização relativa/absoluta do screenshoot
	 * @param errorDetail
	 *            Detalhes do erro
	 * @throws InvalidFormatException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void addTestError(String deviceName, String testCaseName, File screenshot, String started_at,
			String finished_at, String index, String errorDetail)
			throws InvalidFormatException, FileNotFoundException, IOException {
		setTestErrorsQuantity(getTestErrorsQuantity() + 1);
		XWPFParagraph mainParagraph = this.createParagraph();
		mainParagraph.setAlignment(ParagraphAlignment.CENTER);
		XWPFRun titleRun = mainParagraph.createRun();
		titleRun.setFontSize(15);
		titleRun.setText("MOBILE: " + testCaseName + " - " + deviceName);
		titleRun.setBold(true);
		XWPFParagraph timeDetailed = this.createParagraph();
		timeDetailed.setAlignment(ParagraphAlignment.LEFT);
		XWPFRun detailedRun = timeDetailed.createRun();
		detailedRun.setText("Iniciado em: " + started_at);
		detailedRun.addBreak();
		detailedRun.setText("Finalizado em: " + finished_at);
		detailedRun.addBreak();
		detailedRun.setText("Índice: " + index);
		XWPFParagraph paragraph = this.createParagraph();
		paragraph.setAlignment(ParagraphAlignment.CENTER);
		applyBorders(paragraph);
		XWPFRun run = paragraph.createRun();
		run.setFontSize(13);
		run.setItalic(true);
		run.setColor("FF0000");
		run.setText(extractErrorDetail(replaceAllConflictedCharacteres(errorDetail)));
		//run.setText(extractErrorDetail(errorDetail));
		run.addBreak();
		if (screenshot != null && screenshot.exists())
			run.addPicture(new FileInputStream(screenshot), XWPFDocument.PICTURE_TYPE_PNG, screenshot.getName(),
					Units.toEMU(250), Units.toEMU(460));
	}

	private void applyBorders(XWPFParagraph paragraph) {
		paragraph.setBorderBottom(Borders.CELTIC_KNOTWORK);
		paragraph.setBorderTop(Borders.CELTIC_KNOTWORK);
		paragraph.setBorderLeft(Borders.CELTIC_KNOTWORK);
		paragraph.setBorderRight(Borders.CELTIC_KNOTWORK);
	}

	/**
	 * Usado para adicionar erros em testes voltados para browser
	 * 
	 * @param testCaseName
	 *            Nome do caso de teste
	 * @param screenshot
	 *            Localização relativa/absoluta do screenshoot
	 * @param errorDetail
	 *            Detalhes do erro
	 * @throws InvalidFormatException
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public void addTestError(String testCaseName, File screenshot, String started_at, String finished_at, String index,
			String errorDetail) throws InvalidFormatException, FileNotFoundException, IOException {
		setTestErrorsQuantity(getTestErrorsQuantity() + 1);
		XWPFParagraph mainParagraph = this.createParagraph();
		mainParagraph.setAlignment(ParagraphAlignment.CENTER);
		XWPFRun titleRun = mainParagraph.createRun();
		titleRun.setFontSize(15);
		titleRun.setText("WEB: " + testCaseName);
		titleRun.setBold(true);
		XWPFParagraph timeDetailed = this.createParagraph();
		timeDetailed.setAlignment(ParagraphAlignment.LEFT);
		XWPFRun detailedRun = timeDetailed.createRun();
		detailedRun.setText("Iniciado em: " + started_at);
		detailedRun.addBreak();
		detailedRun.setText("Finalizado em: " + finished_at);
		detailedRun.addBreak();
		detailedRun.setText("Índice: " + index);
		XWPFParagraph paragraph = this.createParagraph();
		paragraph.setAlignment(ParagraphAlignment.CENTER);
		applyBorders(paragraph);
		XWPFRun run = paragraph.createRun();
		run.setFontSize(13);
		run.setItalic(true);
		run.setColor("FF0000");
		run.setText(extractErrorDetail(replaceAllConflictedCharacteres(errorDetail)));
		//run.setText(extractErrorDetail(errorDetail));
		//run.addBreak();
		if (screenshot != null && screenshot.exists())
			run.addPicture(new FileInputStream(screenshot), XWPFDocument.PICTURE_TYPE_PNG, screenshot.getName(),
					Units.toEMU(515), Units.toEMU(370));
	}
	
	private String replaceAllConflictedCharacteres(String string){
		//return string.replace("Ã£", "a").replace("ã", "a").replace("Ã§", "c").replace("ç", "c");
		return string.replace("Ã£", "ã").replace("Ã§", "ç");
	}

	public void breakPage() {
		XWPFRun lastRun = this.createParagraph().createRun();
		lastRun.addBreak(BreakType.PAGE);
	}

	public Integer getTestErrorsQuantity() {
		return testErrorsQuantity;
	}

	private void setTestErrorsQuantity(Integer testErrorsQuantity) {
		this.testErrorsQuantity = testErrorsQuantity;
	}
}