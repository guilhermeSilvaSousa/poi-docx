package br.com.rsi.parser;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

import br.com.rsi.parser.testng_elements.Test;
import br.com.rsi.parser.testng_elements.TestMethod;

public abstract class FileParser {

	private ArrayList<Test> testElementList;
	private Document document;
	private File file;
	
	public FileParser(String fileLocation) throws Exception {
		this.file = new File(fileLocation);
		this.testElementList = new ArrayList<Test>();
		this.parseFile();
		setAllTestElements();
	}

	protected abstract void parseFile() throws Exception;
	
	protected Document getDocument(){
		return this.document;
	}
	
	protected void setDocument(Document document){
		this.document = document;
	}
	
	protected File getFile(){
		return this.file;
	}
	
	public String getFileLocation(){
		return this.file.getAbsolutePath();
	}

	protected ArrayList<Test> getTestElementList() {
		return testElementList;
	}

	protected void setTestElementList(ArrayList<Test> testElementList) {
		this.testElementList = testElementList;
	}
	
	public Integer getTestElementsQuantity() {
		return getTestElementList().size();
	}
	
	public Integer getAllFailedTestMethodsElementsQuantity(){
		Integer quantity = 0;
		for(Test test : getAllTestElements()){
			for(TestMethod testMethod : test.getFailedTestMethods()){
				quantity ++;
			}
		}
		return quantity;
	}
	
	public Integer getAllPassedTestMethodsElementsQuantity(){
		Integer quantity = 0;
		for(Test test : getAllTestElements()){
			for(TestMethod testMethod : test.getPassedTestMethods()){
				quantity ++;
			}
		}
		return quantity;
	}
	
	public ArrayList<Test> getAllTestElements(){
		return getTestElementList();
	}
	
	protected abstract ArrayList<Element> setTestMethodElements(Element testElement, boolean isAfailedTestMethod);

	public abstract String getSuiteName();
	
	protected abstract void setAllTestElements();
	
	public abstract String getSuiteStarted_at();
	
	public abstract String getSuiteFinished_at();
}
