package br.com.rsi.parser.testng_elements;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class TestMethod {

	private String status, name, started_at, finished_at, errorMessage, fullErrorMessage, index; 
	private int duration;
	private boolean hasErrorMessage;
	
	private TestMethod(Element element){
		setStatus(element.attr("status"));
		setName(element.attr("name"));
		setStarted_at(element.attr("started-at"));
		setFinished_at(element.attr("finished-at"));
		setDuration(Integer.parseInt(element.attr("duration-ms")));
		setIndex(getElementIndex(element));
	}
	
	public static TestMethod convertElementToFailedTestMethod(Element element){
		TestMethod testMethod =  new TestMethod(element);
		testMethod.setErrorMessage(element.getElementsByTag("message"));
		testMethod.setFullErrorMessage(element.getElementsByTag("full-stacktrace").get(0).ownText());
		return testMethod;
	}

	public static TestMethod convertElementToPassedTestMethod(Element element){
		return new TestMethod(element);
	}
	
	private String getElementIndex(Element element){
		Pattern pattern = Pattern.compile(".*#=(\\S+),.*");
		Matcher matcher = pattern.matcher(element.getElementsByTag("param").get(0).getElementsByTag("value").get(0).ownText());
		if(matcher.matches())
			return matcher.group(1);
		throw new RuntimeException("Index not found");
	}
	
	private static String formatDateTime(String dateTime){
		Pattern dateTimePattern = Pattern.compile("^(\\d{4})\\-(\\d{2})\\-(\\d{2})T(.*)Z$"); //ano mês dia
		Matcher matcher = dateTimePattern.matcher(dateTime);
		if(matcher.matches())
			return String.format("%s/%s/%s - (%s)", matcher.group(3), matcher.group(2), matcher.group(1), matcher.group(4));
		return null;
	}
	
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStarted_at() {
		return started_at;
	}

	public void setStarted_at(String started_at) {
		this.started_at = formatDateTime(started_at);
	}

	public String getFinished_at() {
		return finished_at;
	}

	public void setFinished_at(String finished_at) {
		this.finished_at = formatDateTime(finished_at);
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = (int)(duration / 1000);
	}
	
	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	public String getErrorMessage() {
		if(hasErrorMessage)
			return errorMessage;
		return fullErrorMessage;
	}

	public void setErrorMessage(Elements errorMessageElement) {
		if(errorMessageElement.size() > 0){
			this.errorMessage = errorMessageElement.get(0).ownText();
			hasErrorMessage = true;
		}else{
			hasErrorMessage = false;
		}
	}

	public String getFullErrorMessage() {
		return fullErrorMessage;
	}

	public void setFullErrorMessage(String fullErrorMessage) {
		this.fullErrorMessage = fullErrorMessage;
	}

	public String getFolderName(){
		return this.getName() + "_" + getIndex();
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("TestMethod Name = " + this.getName() + "\n");
		sb.append("Status = " + this.getStatus() + "\n");
		sb.append("Index = " + getIndex() + "\n");
		if(hasErrorMessage)sb.append("Error message = " + getErrorMessage() + "\n");
		sb.append("Full error message = " + getFullErrorMessage() + "\n");
		sb.append("Started at = " + this.getStarted_at() + "\n");
		sb.append("Finished at = " + this.getFinished_at() + "\n");
		sb.append("Duration = " + this.getDuration() + "\n");
		return sb.toString();
	}
}
