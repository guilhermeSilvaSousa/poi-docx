package br.com.rsi.parser.testng_elements;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.nodes.Element;

public class Test {

	private boolean isMobileTest;
	private ArrayList<TestMethod> failedTestMethods;
	private ArrayList<TestMethod> passedTestMethods;
	private String name, started_at, finished_at, deviceName;
	private int duration;
	
	private Test(Element element) {
		setName(element.attr("name"));
		setStarted_at(element.attr("started-at"));
		setFinished_at(element.attr("finished-at"));
		setDuration(Integer.parseInt(element.attr("duration-ms")));
		this.failedTestMethods = new ArrayList<TestMethod>();
		this.passedTestMethods = new ArrayList<TestMethod>();
		setTestTarget();
	}

	public static Test convertElementToTest(Element element) {
		return new Test(element);
	}
	
	private void setTestTarget() {
		Pattern pattern = Pattern.compile("^.*Mobile.*\\((.*)\\).*$");
		Matcher matcher = null;
		matcher = pattern.matcher(getName());
		if (matcher.matches()) {
			setDeviceName(matcher.group(1));
			setMobileTest(true);
		} else {
			setMobileTest(false);
		}
	}

	public void addFailedTestMethod(TestMethod testMethod) {
		this.failedTestMethods.add(testMethod);
	}

	public void addPassedTestMethod(TestMethod testMethod) {
		this.passedTestMethods.add(testMethod);
	}
	
	public boolean isMobileTest() {
		return isMobileTest;
	}

	public void setMobileTest(boolean isMobileTest) {
		this.isMobileTest = isMobileTest;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getStarted_at() {
		return started_at;
	}

	public void setStarted_at(String started_at) {
		this.started_at = formatDateTime(started_at);
	}
	
	private static String formatDateTime(String dateTime){
		Pattern dateTimePattern = Pattern.compile("^(\\d{4})\\-(\\d{2})\\-(\\d{2})T(.*)Z$"); //ano mês dia
		Matcher matcher = dateTimePattern.matcher(dateTime);
		if(matcher.matches())
			return String.format("%s/%s/%s - (%s)", matcher.group(3), matcher.group(2), matcher.group(1), matcher.group(4));
		return null;
	}

	public String getFinished_at() {
		return finished_at;
	}

	public void setFinished_at(String finished_at) {
		this.finished_at = formatDateTime(finished_at);
	}

	public String getDeviceName() {
		return deviceName;
	}

	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	public int getDuration() {
		return duration;
	}

	public void setDuration(int duration) {
		this.duration = (int)(duration / 1000);
	}

	public ArrayList<TestMethod> getFailedTestMethods(){
		return this.failedTestMethods;
	}
	
	public ArrayList<TestMethod> getPassedTestMethods(){
		return this.passedTestMethods;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Test Name = " + getName() + "\n");
		sb.append("Started at = " + getStarted_at() + "\n");
		sb.append("Finished at = " + getFinished_at() + "\n");
		sb.append("Is Mobile test = " + isMobileTest() + "\n");
		if(this.isMobileTest)sb.append("Device name = " + getDeviceName() + "\n");
		sb.append("Failed Test methods = " + getName() + "\n");
		for (TestMethod testMethod : this.failedTestMethods) {
			sb.append("-------------------------------------------\n");
			sb.append(testMethod);
			sb.append("-------------------------------------------\n");
		}
		sb.append("===============================================\n");
		sb.append("Passed Test methods = " + getName() + "\n");
		for (TestMethod testMethod : this.passedTestMethods) {
			sb.append("-------------------------------------------\n");
			sb.append(testMethod);
			sb.append("-------------------------------------------\n");
		}
		sb.append("===============================================\n");
		return sb.toString();
	}
}
