package br.com.rsi.parser;

import java.io.FileInputStream;
import java.util.ArrayList;

import org.apache.poi.util.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;

public class HtmlFileParser extends FileParser {

	public HtmlFileParser(String fileLocation) throws Exception {
		super(fileLocation);
	}

	@Override
	public void parseFile() throws Exception {
		String fileContent = new String(IOUtils.toByteArray(new FileInputStream(getFile())));
		setDocument(Jsoup.parse(fileContent, "", Parser.htmlParser()));
	}

	@Override
	protected void setAllTestElements() {
	}

	@Override
	public String getSuiteStarted_at() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getSuiteFinished_at() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	protected ArrayList<Element> setTestMethodElements(Element testElement, boolean isAfailedTestMethod) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getSuiteName() {
		// TODO Auto-generated method stub
		return null;
	}
}
