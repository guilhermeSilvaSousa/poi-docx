package br.com.rsi.parser;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.util.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.parser.Parser;
import org.jsoup.select.Elements;

import br.com.rsi.parser.testng_elements.Test;
import br.com.rsi.parser.testng_elements.TestMethod;

public class XMlFileParser extends FileParser {
	
	public XMlFileParser(String fileLocation) throws Exception {
		super(fileLocation);
	}

	public static void main(String[] args) throws Exception{
		XMlFileParser xmlFileParser = new XMlFileParser("C:\\Users\\guilherme.sousa\\Desktop\\AUTOMACAO\\EXECUCAO\\0.config\\test-output\\testng-results.xml");
/*		for(Test test : xmlFileParser.getAllFailedTestElements()){
			System.out.println(test);
		}*/
		System.out.println(xmlFileParser.getSuiteStarted_at());
		System.out.println(xmlFileParser.getSuiteFinished_at());
	}
	
	private static String formatDateTime(String dateTime){
		Pattern dateTimePattern = Pattern.compile("^(\\d{4})\\-(\\d{2})\\-(\\d{2})T(.*)Z$"); //ano mês dia
		Matcher matcher = dateTimePattern.matcher(dateTime);
		if(matcher.matches())
			return String.format("%s/%s/%s - (%s)", matcher.group(3), matcher.group(2), matcher.group(1), matcher.group(4));
		return dateTime;
	}
	
	@Override
	public void parseFile() throws Exception {
		String fileContent = new String(IOUtils.toByteArray(new FileInputStream(getFile())));
		setDocument(Jsoup.parse(fileContent, "", Parser.xmlParser()));
	}

	@Override
	protected ArrayList<Element> setTestMethodElements(Element testElement, boolean isAfailedTestMethod) {
		ArrayList<Element> testMethods = new ArrayList<Element>();
		Elements elements = null;
		if(isAfailedTestMethod){
			elements = testElement.getElementsByAttributeValue("status", "FAIL");
		}else{
			elements = testElement.getElementsByAttributeValue("status", "PASS");
		}
		for (Element element : elements) {
			if (element.hasAttr("data-provider")) {
				testMethods.add(element);
			}
		}
		return testMethods;
	}

	@Override
	protected void setAllTestElements() {
		Test test = null;
		boolean failedTestMethodFound = false;
		Elements testElements = getDocument().getElementsByTag("test");
		for (Element testElement : testElements) {
			test = Test.convertElementToTest(testElement);
			for (Element testMethod : setTestMethodElements(testElement, true)) {
				test.addFailedTestMethod(TestMethod.convertElementToFailedTestMethod(testMethod));
				failedTestMethodFound = true;
			}
			for (Element testMethod : setTestMethodElements(testElement, false)) {
				test.addPassedTestMethod(TestMethod.convertElementToPassedTestMethod(testMethod));
			}
			//if(failedTestMethodFound)
				getTestElementList().add(test);
			//failedTestMethodFound = false;
		}
	}
	
	@Override
	public String getSuiteStarted_at(){
		ArrayList<String> started_atList = new ArrayList<String>();
		String[] started_atArray = null;
		Elements elements = getDocument().getElementsByTag("test-method");
		for(Element element : elements){
			started_atList.add(element.attr("started-at"));
		}
		started_atArray = stringArrayListToArray(started_atList);
		if(started_atArray.length != 0){
			Arrays.sort(started_atArray);
			return formatDateTime(started_atArray[0]);
		}else{
			return null;	
		}
	}
	
	@Override
	public String getSuiteFinished_at(){
		ArrayList<String> finished_atList = new ArrayList<String>();
		String[] finished_atArray = null;
		Elements elements = getDocument().getElementsByTag("test-method");
		for(Element element : elements){
			finished_atList.add(element.attr("finished-at"));
		}
		finished_atArray = stringArrayListToArray(finished_atList);
		if(finished_atArray.length != 0){
			Arrays.sort(finished_atArray);
			return formatDateTime(finished_atArray[finished_atArray.length-1]);
		}else{
			return null;	
		}
	}
	
	private String[] stringArrayListToArray(ArrayList<String> stringArrayList){
		String[] stringArray = new String[stringArrayList.size()];
		for(int i = 0; i < stringArray.length; i++){
			stringArray[i] = stringArrayList.get(i);
		}
		return stringArray;
	}

	@Override
	public String getSuiteName() {
		Elements elements = getDocument().getElementsByTag("suite");
		return elements.attr("name");
	}
}
