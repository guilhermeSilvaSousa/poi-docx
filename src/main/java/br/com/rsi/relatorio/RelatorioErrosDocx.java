package br.com.rsi.relatorio;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import br.com.rsi.parser.FileParser;
import br.com.rsi.parser.XMlFileParser;
import br.com.rsi.parser.testng_elements.Test;
import br.com.rsi.parser.testng_elements.TestMethod;
import br.com.rsi.poi.CustomXWPFDocument;

public class RelatorioErrosDocx {

	private String suiteLocation, templateLocation;
	private FileParser fileParser;
	private CustomXWPFDocument document;
	
	public RelatorioErrosDocx(String suiteLocation, String templateLocation, FileParser fileParser) throws InvalidFormatException, FileNotFoundException, IOException{
		setSuiteLocation(suiteLocation);
		setTemplateLocation(templateLocation);
		setFileParser(fileParser);
		setDocument(new CustomXWPFDocument(new FileInputStream(templateLocation), getFileParser().getSuiteName(), getFileParser().getSuiteStarted_at(), getFileParser().getSuiteFinished_at(), getFileParser().getAllPassedTestMethodsElementsQuantity(), getFileParser().getAllFailedTestMethodsElementsQuantity()));
	}
	
	@SuppressWarnings("unused")
	public static void main(String[] args) throws InvalidFormatException, FileNotFoundException, IOException {
		if (args.length == 3) {
			boolean invalidParams = false;
			Pattern suiteLocationPattern, templateLocationPattern, xmlLocationPattern, htmlLocationPattern;
			String suiteLocation, templateLocation, xmlLocation, htmlLocation;
			Matcher matcher = null;
			suiteLocationPattern = Pattern.compile("^suiteLocation=(.*)$");
			templateLocationPattern = Pattern.compile("^templateLocation=(.*)$");
			xmlLocationPattern = Pattern.compile("^xmlLocation=(.*)$");
			htmlLocationPattern = Pattern.compile("^htmlLocation=(.*)$");
			matcher = suiteLocationPattern.matcher(args[0]);
			if(matcher.matches()){
				suiteLocation = matcher.group(1);
				matcher = templateLocationPattern.matcher(args[1]);
				if(matcher.matches()){	
					templateLocation = matcher.group(1);
					matcher = xmlLocationPattern.matcher(args[2]);
					if(matcher.matches()){
						xmlLocation = matcher.group(1);
						try {
							RelatorioErrosDocx relatorioErrosDocx = new RelatorioErrosDocx(suiteLocation, templateLocation, new XMlFileParser(xmlLocation));
							relatorioErrosDocx.setDocumentContent();
						} catch (Exception e) {
							e.printStackTrace();
						}
					}else if((matcher = htmlLocationPattern.matcher(args[2])).matches()){
						htmlLocation = matcher.group(1);
						//Implementar caso necessário
					}else{
						invalidParams = true;
					}
				}else{
					invalidParams = true;
				}
			}else{
				invalidParams = true;
			}
			if(invalidParams)
				System.err.println("Modo de usar:\njava -jar <jar_executable> suiteLocation=<suite_path> templateLocation=<template_path> [xmlLocation=<xml_file_path>] | [htmlLocation=<html_file_location>]\n");
		}else{
			System.err.println("Modo de usar:\njava -jar <jar_executable> suiteLocation=<suite_path> templateLocation=<template_path> [xmlLocation=<xml_file_path>] | [htmlLocation=<html_file_location>]\n");
		}
	}

	public void setDocumentContent() throws InvalidFormatException, FileNotFoundException, IOException{
		File suiteFolder = new File(getSuiteLocation());
		int testIndex = 0;
		if(suiteFolder.exists()){
			File mobileScreenshotsFolder = new File(suiteFolder, "mobile_screenshot");
			File webScreenshotsFolder = new File(suiteFolder, "web_screenshot");
			File deviceFolder = null;
			File testMethodDeviceFolder = null;
			File webTestFolder = null;
			for(Test test : getFileParser().getAllTestElements()){
				if(test.isMobileTest()){
					if(mobileScreenshotsFolder.exists()){
						deviceFolder = new File(mobileScreenshotsFolder, test.getDeviceName()); //O diretório do dispositívo na pasta de mobile_screenshot
						for(TestMethod testMethod : test.getFailedTestMethods()){
							if(testIndex > 0)
								getDocument().breakPage();
							testIndex++;
							testMethodDeviceFolder = new File(deviceFolder, testMethod.getFolderName());
							File failedStepScreenshot = getFailedStepScreenshotFile(testMethodDeviceFolder);
							//if(lastScreenshot != null){
								getDocument().addTestError(test.getDeviceName(), testMethod.getName(), failedStepScreenshot, testMethod.getStarted_at(), testMethod.getFinished_at(), String.valueOf(testMethod.getIndex()), testMethod.getErrorMessage());
							//}
						}
					}
				}else{
					if(webScreenshotsFolder.exists()){
						for(TestMethod testMethod : test.getFailedTestMethods()){
							if(testIndex > 0)
								getDocument().breakPage();
							testIndex++;
							webTestFolder = new File(webScreenshotsFolder, testMethod.getFolderName());
							File lastScreenshot = getFailedStepScreenshotFile(webTestFolder);
							//if(lastScreenshot != null){
								getDocument().addTestError(testMethod.getName(), lastScreenshot, testMethod.getStarted_at(), testMethod.getFinished_at(), String.valueOf(testMethod.getIndex()), testMethod.getErrorMessage());
							//}
						}
					}
				}
			}
			//if(getDocument().getTestErrorsQuantity() > 0){
				getDocument().saveDocument();
			//}
		}
	}
	
	private File getFailedStepScreenshotFile(File dir){
	    File[] files = dir.listFiles();
	    if (files == null || files.length == 0) {
	        return null;
	    }

	/*    File lastModifiedFile = files[0];
	    for (int i = 1; i < files.length; i++) {
	       if (lastModifiedFile.lastModified() < files[i].lastModified()) {
	           lastModifiedFile = files[i];
	       }
	    }*/
	    for(File screenshot : files){
	    	if(screenshot.getName().equals("FAILED_STEP.png"))
	    		return screenshot;
	    }
	    return null;
	}
	
	public String getSuiteLocation() {
		return suiteLocation;
	}

	public void setSuiteLocation(String suiteLocation) {
		this.suiteLocation = suiteLocation;
	}

	public String getTemplateLocation() {
		return templateLocation;
	}

	public void setTemplateLocation(String templateLocation) {
		this.templateLocation = templateLocation;
	}

	public FileParser getFileParser() {
		return fileParser;
	}

	public void setFileParser(FileParser fileParser) {
		this.fileParser = fileParser;
	}

	public CustomXWPFDocument getDocument() {
		return document;
	}

	public void setDocument(CustomXWPFDocument document) {
		this.document = document;
	}
}
